set @n:=0;
SELECT
  	@n:=@n+1 as rownum,
	'ЦАО' AS Округ,
	inventory_spr_location.`name` AS Филиал,
	inventory_spr_unit.`name` AS 'Наименование по бухгалтерии',
	CASE
WHEN inventory_asset.id_unit_id IN (9, 36, 62, 70) THEN
	'2007'
WHEN inventory_asset.id_unit_id IN (39, 42, 77, 78) THEN
	'2008'
WHEN inventory_asset.id_unit_id IN (10, 35, 39, 46, 93) THEN
	'2009'
WHEN inventory_asset.id_unit_id IN (53, 83, 85, 87, 60, 69, 51, 90, 34, 54) THEN
	'2010'
WHEN inventory_asset.id_unit_id IN (99, 100, 101, 106, 105) THEN
	'2013'
WHEN inventory_asset.id_status_id = 4
AND inventory_asset.id_unit_id = 68 THEN
	'2013'
ELSE
	''
END AS 'год вып',
 inventory_asset.number as 'Инв.№',
'S/N если используется',
 inventory_spr_type.`name` as 'Краткое название',
 inventory_asset.note as 'Примечание'
FROM
	inventory_asset
LEFT JOIN inventory_spr_location ON inventory_spr_location.id = inventory_asset.id_location_id
LEFT JOIN inventory_spr_status ON inventory_spr_status.id = inventory_asset.id_status_id
LEFT JOIN inventory_spr_type ON inventory_spr_type.id = inventory_asset.id_type_id
LEFT JOIN inventory_spr_unit ON inventory_spr_unit.id = inventory_asset.id_unit_id
